# README #

### What is this repository for? ###

* This repository contains edited version of the WhyNot English subtitles for the original Steins;Gate anime. 
* The subtitles are timed for the video release that doesn't have the opening at the beginning
* The changes that have been made to the subs were mainly made for reasons of consistency with the Steins;Gate Visual Novel
* They include things like "Luka" instead of "Ruka" as it is the official translation from the authors

### Who do I talk to? ###

* You can contact Woute#4407 or Davixxa#4194 on Discord for any inquiry